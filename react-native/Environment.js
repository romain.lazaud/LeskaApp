const ENV = {
  dev: {
    apiUrl: 'http://localhost:44360',
    oAuthConfig: {
      issuer: 'http://localhost:44393',
      clientId: 'LeskaApp_App',
      clientSecret: '1q2w3e*',
      scope: 'offline_access LeskaApp',
    },
    localization: {
      defaultResourceName: 'LeskaApp',
    },
  },
  prod: {
    apiUrl: 'http://localhost:44360',
    oAuthConfig: {
      issuer: 'http://localhost:44393',
      clientId: 'LeskaApp_App',
      clientSecret: '1q2w3e*',
      scope: 'offline_access LeskaApp',
    },
    localization: {
      defaultResourceName: 'LeskaApp',
    },
  },
};

export const getEnvVars = () => {
  // eslint-disable-next-line no-undef
  return __DEV__ ? ENV.dev : ENV.prod;
};
