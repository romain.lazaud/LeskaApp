import { Environment } from '@abp/ng.core';

const baseUrl = 'http://localhost:4200';

export const environment = {
  production: true,
  application: {
    baseUrl,
    name: 'LeskaApp',
    logoUrl: '',
  },
  oAuthConfig: {
    issuer: 'https://localhost:44393',
    redirectUri: baseUrl,
    clientId: 'LeskaApp_App',
    responseType: 'code',
    scope: 'offline_access LeskaApp',
    requireHttps: true
  },
  apis: {
    default: {
      url: 'https://localhost:44360',
      rootNamespace: 'RomLaz.LeskaApp',
    },
  },
} as Environment;
