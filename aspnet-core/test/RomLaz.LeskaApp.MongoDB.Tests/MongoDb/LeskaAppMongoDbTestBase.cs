﻿// <copyright file="LeskaAppMongoDbTestBase.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.MongoDB
{
    public abstract class LeskaAppMongoDbTestBase : LeskaAppTestBase<LeskaAppMongoDbTestModule> 
    {

    }
}
