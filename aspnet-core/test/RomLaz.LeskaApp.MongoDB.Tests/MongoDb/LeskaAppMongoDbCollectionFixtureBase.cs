﻿// <copyright file="LeskaAppMongoDbCollectionFixtureBase.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.MongoDB
{
    using RomLaz.LeskaApp.MongoDB;
    using Xunit;

    public class LeskaAppMongoDbCollectionFixtureBase : ICollectionFixture<LeskaAppMongoDbFixture>
    {

    }
}
