﻿// <copyright file="LeskaAppMongoDbTestModule.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.MongoDB
{
    using System;
    using Volo.Abp.Data;
    using Volo.Abp.Modularity;

    [DependsOn(
        typeof(LeskaAppTestBaseModule),
        typeof(LeskaAppMongoDbModule))]
    public class LeskaAppMongoDbTestModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            var stringArray = LeskaAppMongoDbFixture.ConnectionString.Split('?');
                        var connectionString = stringArray[0].EnsureEndsWith('/')  +
                                                   "Db_" +
                                               Guid.NewGuid().ToString("N") + "/?" + stringArray[1];

            this.Configure<AbpDbConnectionOptions>(options =>
            {
                options.ConnectionStrings.Default = connectionString;
            });
        }
    }
}
