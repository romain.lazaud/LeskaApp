﻿// <copyright file="LeskaAppMongoCollection.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.MongoDB
{
    using Xunit;

    [CollectionDefinition(LeskaAppTestConsts.CollectionDefinitionName)]
    public class LeskaAppMongoCollection : LeskaAppMongoDbCollectionFixtureBase
    {

    }
}
