﻿// <copyright file="SampleRepositoryTests.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.MongoDB.Samples
{
    using System;
    using System.Threading.Tasks;
    using global::MongoDB.Driver.Linq;
    using Shouldly;
    using Volo.Abp.Domain.Repositories;
    using Volo.Abp.Identity;
    using Xunit;

    /* This is just an example test class.
     * Normally, you don't test ABP framework code
     * (like default AppUser repository IRepository<AppUser, Guid> here).
     * Only test your custom repository methods.
     */
    [Collection(LeskaAppTestConsts.CollectionDefinitionName)]
    public class SampleRepositoryTests
        : LeskaAppMongoDbTestBase
    {
        private readonly IRepository<IdentityUser, Guid> appUserRepository;

        public SampleRepositoryTests()
        {
            this.appUserRepository = this.GetRequiredService<IRepository<IdentityUser, Guid>>();
        }

        [Fact]
        public async Task Should_Query_AppUser()
        {
            /* Need to manually start Unit Of Work because
             * FirstOrDefaultAsync should be executed while db connection / context is available.
             */
            await this.WithUnitOfWorkAsync(async () =>
            {
                //Act
                var adminUser = await (await this.appUserRepository.GetMongoQueryableAsync())
                    .FirstOrDefaultAsync(u => u.UserName == "admin");

                //Assert
                adminUser.ShouldNotBeNull();
            });
        }
    }
}
