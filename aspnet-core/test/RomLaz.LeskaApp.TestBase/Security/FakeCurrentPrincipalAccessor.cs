﻿// <copyright file="FakeCurrentPrincipalAccessor.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Security
{
    using System.Collections.Generic;
    using System.Security.Claims;
    using Volo.Abp.DependencyInjection;
    using Volo.Abp.Security.Claims;

    [Dependency(ReplaceServices = true)]
    public class FakeCurrentPrincipalAccessor : ThreadCurrentPrincipalAccessor
    {
        protected override ClaimsPrincipal GetClaimsPrincipal()
        {
            return this.GetPrincipal();
        }

        private ClaimsPrincipal principal;

        private ClaimsPrincipal GetPrincipal()
        {
            if (this.principal == null)
            {
                lock (this)
                {
                    if (this.principal == null)
                    {
                        this.principal = new ClaimsPrincipal(
                            new ClaimsIdentity(
                                new List<Claim>
                                {
                                    new Claim(AbpClaimTypes.UserId,"2e701e62-0953-4dd3-910b-dc6cc93ccb0d"),
                                    new Claim(AbpClaimTypes.UserName,"admin"),
                                    new Claim(AbpClaimTypes.Email,"admin@abp.io"),
                                }));
                    }
                }
            }

            return this.principal;
        }
    }
}
