﻿// <copyright file="LeskaAppTestConsts.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp
{
    public static class LeskaAppTestConsts
    {
        public const string CollectionDefinitionName = "LeskaApp collection";
    }
}
