﻿// <copyright file="ClientDemoService.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.HttpApi.Client.ConsoleTestApp
{
    using System;
    using System.Threading.Tasks;
    using Volo.Abp.Account;
    using Volo.Abp.DependencyInjection;

    public class ClientDemoService : ITransientDependency
    {
        private readonly IProfileAppService profileAppService;

        public ClientDemoService(IProfileAppService profileAppService)
        {
            this.profileAppService = profileAppService;
        }

        public async Task RunAsync()
        {
            var output = await this.profileAppService.GetAsync();
            Console.WriteLine($"UserName : {output.UserName}");
            Console.WriteLine($"Email    : {output.Email}");
            Console.WriteLine($"Name     : {output.Name}");
            Console.WriteLine($"Surname  : {output.Surname}");
        }
    }
}