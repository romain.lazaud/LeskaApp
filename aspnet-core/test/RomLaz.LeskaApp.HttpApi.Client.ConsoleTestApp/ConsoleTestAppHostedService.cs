// <copyright file="ConsoleTestAppHostedService.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.HttpApi.Client.ConsoleTestApp
{
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Volo.Abp;

    public class ConsoleTestAppHostedService : IHostedService
    {
        private readonly IConfiguration configuration;

        public ConsoleTestAppHostedService(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            using (var application = AbpApplicationFactory.Create<LeskaAppConsoleApiClientModule>(options =>
            {
                options.Services.ReplaceConfiguration(this.configuration);
                options.UseAutofac();
            }))
            {
                application.Initialize();

                var demo = application.ServiceProvider.GetRequiredService<ClientDemoService>();
                await demo.RunAsync();

                application.Shutdown();
            }
        }

        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
    }
}
