﻿// <copyright file="LeskaAppConsoleApiClientModule.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.HttpApi.Client.ConsoleTestApp
{
    using System;
    using Microsoft.Extensions.DependencyInjection;
    using Polly;
    using Volo.Abp.Autofac;
    using Volo.Abp.Http.Client;
    using Volo.Abp.Http.Client.IdentityModel;
    using Volo.Abp.Modularity;

    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(LeskaAppHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule))]
    public class LeskaAppConsoleApiClientModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            this.PreConfigure<AbpHttpClientBuilderOptions>(options =>
            {
                options.ProxyClientBuildActions.Add((remoteServiceName, clientBuilder) =>
                {
                    clientBuilder.AddTransientHttpErrorPolicy(
                        policyBuilder => policyBuilder.WaitAndRetryAsync(3, i => TimeSpan.FromSeconds(Math.Pow(2, i))));
                });
            });
        }
    }
}
