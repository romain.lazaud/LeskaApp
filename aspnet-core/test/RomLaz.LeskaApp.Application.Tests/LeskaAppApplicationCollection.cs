// <copyright file="LeskaAppApplicationCollection.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp
{
    using RomLaz.LeskaApp.MongoDB;
    using Xunit;

    [CollectionDefinition(LeskaAppTestConsts.CollectionDefinitionName)]
    public class LeskaAppApplicationCollection : LeskaAppMongoDbCollectionFixtureBase
    {

    }
}
