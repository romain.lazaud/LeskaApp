﻿// <copyright file="SampleAppServiceTests.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Samples
{
    using System.Threading.Tasks;
    using Shouldly;
    using Volo.Abp.Identity;
    using Xunit;

    /* This is just an example test class.
     * Normally, you don't test code of the modules you are using
     * (like IIdentityUserAppService here).
     * Only test your own application services.
     */
    [Collection(LeskaAppTestConsts.CollectionDefinitionName)]
    public class SampleAppServiceTests : LeskaAppApplicationTestBase
    {
        private readonly IIdentityUserAppService userAppService;

        public SampleAppServiceTests()
        {
            this.userAppService = this.GetRequiredService<IIdentityUserAppService>();
        }

        [Fact]
        public async Task Initial_Data_Should_Contain_Admin_User()
        {
            //Act
            var result = await this.userAppService.GetListAsync(new GetIdentityUsersInput());

            //Assert
            result.TotalCount.ShouldBeGreaterThan(0);
            result.Items.ShouldContain(u => u.UserName == "admin");
        }
    }
}
