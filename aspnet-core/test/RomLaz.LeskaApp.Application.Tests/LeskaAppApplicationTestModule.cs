﻿// <copyright file="LeskaAppApplicationTestModule.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp
{
    using Volo.Abp.Modularity;

    [DependsOn(
        typeof(LeskaAppApplicationModule),
        typeof(LeskaAppDomainTestModule))]
    public class LeskaAppApplicationTestModule : AbpModule
    {

    }
}