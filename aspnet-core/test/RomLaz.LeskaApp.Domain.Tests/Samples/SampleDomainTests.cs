﻿// <copyright file="SampleDomainTests.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Samples
{
    using System.Threading.Tasks;
    using Shouldly;
    using Volo.Abp.Identity;
    using Xunit;

    /* This is just an example test class.
     * Normally, you don't test code of the modules you are using
     * (like IdentityUserManager here).
     * Only test your own domain services.
     */
    [Collection(LeskaAppTestConsts.CollectionDefinitionName)]
    public class SampleDomainTests : LeskaAppDomainTestBase
    {
        private readonly IIdentityUserRepository identityUserRepository;
        private readonly IdentityUserManager identityUserManager;

        public SampleDomainTests()
        {
            this.identityUserRepository = this.GetRequiredService<IIdentityUserRepository>();
            this.identityUserManager = this.GetRequiredService<IdentityUserManager>();
        }

        [Fact]
        public async Task Should_Set_Email_Of_A_User()
        {
            IdentityUser adminUser;

            /* Need to manually start Unit Of Work because
             * FirstOrDefaultAsync should be executed while db connection / context is available.
             */
            await this.WithUnitOfWorkAsync(async () =>
            {
                adminUser = await this.identityUserRepository
                    .FindByNormalizedUserNameAsync("ADMIN");

                await this.identityUserManager.SetEmailAsync(adminUser, "newemail@abp.io");
                await this.identityUserRepository.UpdateAsync(adminUser);
            });

            adminUser = await this.identityUserRepository.FindByNormalizedUserNameAsync("ADMIN");
            adminUser.Email.ShouldBe("newemail@abp.io");
        }
    }
}
