// <copyright file="LeskaAppDomainTestModule.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp
{
    using RomLaz.LeskaApp.MongoDB;
    using Volo.Abp.Modularity;

    [DependsOn(
        typeof(LeskaAppMongoDbTestModule))]
    public class LeskaAppDomainTestModule : AbpModule
    {

    }
}