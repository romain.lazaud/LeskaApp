// <copyright file="Index.cshtml.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Pages
{
    using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

    public class IndexModel : AbpPageModel
    {
        public void OnGet()
        {
        }
    }
}