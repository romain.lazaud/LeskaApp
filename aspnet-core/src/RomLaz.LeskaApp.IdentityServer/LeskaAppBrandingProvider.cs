﻿// <copyright file="LeskaAppBrandingProvider.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp
{
    using Volo.Abp.DependencyInjection;
    using Volo.Abp.Ui.Branding;

    [Dependency(ReplaceServices = true)]
    public class LeskaAppBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "LeskaApp";
    }
}
