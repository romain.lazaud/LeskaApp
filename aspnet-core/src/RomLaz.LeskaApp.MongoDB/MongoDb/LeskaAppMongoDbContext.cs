﻿// <copyright file="LeskaAppMongoDbContext.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.MongoDB
{
    using global::MongoDB.Driver;
    using RomLaz.LeskaApp.Questions;
    using Volo.Abp.Data;
    using Volo.Abp.MongoDB;

    [ConnectionStringName("Default")]
    public class LeskaAppMongoDbContext : AbpMongoDbContext
    {
        public IMongoCollection<QuestionThread> Books => this.Collection<QuestionThread>();

        protected override void CreateModel(IMongoModelBuilder modelBuilder)
        {
            base.CreateModel(modelBuilder);

            //builder.Entity<YourEntity>(b =>
            //{
            //    //...
            //});
        }
    }
}
