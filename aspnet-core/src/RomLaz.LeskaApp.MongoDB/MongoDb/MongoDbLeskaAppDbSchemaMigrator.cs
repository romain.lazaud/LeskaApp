﻿// <copyright file="MongoDbLeskaAppDbSchemaMigrator.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.MongoDB
{
    using System;
    using System.Threading.Tasks;
    using global::MongoDB.Driver;
    using Microsoft.Extensions.DependencyInjection;
    using RomLaz.LeskaApp.Data;
    using Volo.Abp.Data;
    using Volo.Abp.DependencyInjection;
    using Volo.Abp.MongoDB;

    public class MongoDbLeskaAppDbSchemaMigrator
        : ILeskaAppDbSchemaMigrator, ITransientDependency
    {
        private readonly IServiceProvider serviceProvider;

        public MongoDbLeskaAppDbSchemaMigrator(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public async Task MigrateAsync()
        {
            var dbContexts = this.serviceProvider.GetServices<IAbpMongoDbContext>();
            var connectionStringResolver = this.serviceProvider.GetRequiredService<IConnectionStringResolver>();

            foreach (var dbContext in dbContexts)
            {
                var connectionString =
                    await connectionStringResolver.ResolveAsync(
                        ConnectionStringNameAttribute.GetConnStringName(dbContext.GetType()));
                var mongoUrl = new MongoUrl(connectionString);
                var databaseName = mongoUrl.DatabaseName;
                var client = new MongoClient(mongoUrl);

                if (databaseName.IsNullOrWhiteSpace())
                {
                    databaseName = ConnectionStringNameAttribute.GetConnStringName(dbContext.GetType());
                }

                (dbContext as AbpMongoDbContext)?.InitializeCollections(client.GetDatabase(databaseName));
            }
        }
    }
}
