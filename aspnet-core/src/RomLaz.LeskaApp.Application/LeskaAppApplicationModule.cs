﻿// <copyright file="LeskaAppApplicationModule.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp
{
    using Volo.Abp.Account;
    using Volo.Abp.AutoMapper;
    using Volo.Abp.FeatureManagement;
    using Volo.Abp.Identity;
    using Volo.Abp.Modularity;
    using Volo.Abp.PermissionManagement;
    using Volo.Abp.SettingManagement;
    using Volo.Abp.TenantManagement;

    [DependsOn(
        typeof(LeskaAppDomainModule),
        typeof(AbpAccountApplicationModule),
        typeof(LeskaAppApplicationContractsModule),
        typeof(AbpIdentityApplicationModule),
        typeof(AbpPermissionManagementApplicationModule),
        typeof(AbpTenantManagementApplicationModule),
        typeof(AbpFeatureManagementApplicationModule),
        typeof(AbpSettingManagementApplicationModule))]
    public class LeskaAppApplicationModule
        : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            this.Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddMaps<LeskaAppApplicationModule>();
            });
        }
    }
}
