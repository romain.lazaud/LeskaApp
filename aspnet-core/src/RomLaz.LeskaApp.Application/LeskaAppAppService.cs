﻿// <copyright file="LeskaAppAppService.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using RomLaz.LeskaApp.Localization;
    using Volo.Abp.Application.Services;

    /* Inherit your application services from this class.
     */
    public abstract class LeskaAppAppService : ApplicationService
    {
        protected LeskaAppAppService()
        {
            this.LocalizationResource = typeof(LeskaAppResource);
        }
    }
}
