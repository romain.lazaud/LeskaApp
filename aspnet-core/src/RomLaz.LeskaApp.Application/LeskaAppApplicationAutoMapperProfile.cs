﻿// <copyright file="LeskaAppApplicationAutoMapperProfile.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp
{
    using AutoMapper;

    public class LeskaAppApplicationAutoMapperProfile
        : AutoMapper.Profile
    {
        public LeskaAppApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}
