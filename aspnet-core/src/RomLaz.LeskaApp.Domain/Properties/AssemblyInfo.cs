﻿// <copyright file="AssemblyInfo.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

using System.Runtime.CompilerServices;
[assembly:InternalsVisibleToAttribute("RomLaz.LeskaApp.Domain.Tests")]
[assembly:InternalsVisibleToAttribute("RomLaz.LeskaApp.TestBase")]
