﻿// <copyright file="LeskaAppDataSeedContributor.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Data
{
    using System;
    using System.Threading.Tasks;
    using RomLaz.LeskaApp.Questions;
    using Volo.Abp.Data;
    using Volo.Abp.DependencyInjection;
    using Volo.Abp.Domain.Repositories;

    public class LeskaAppDataSeedContributor
        : IDataSeedContributor, ITransientDependency
    {
        private readonly IRepository<QuestionThread, Guid> questionRepository;

        public LeskaAppDataSeedContributor(IRepository<QuestionThread, Guid> questionRepository)
        {
            this.questionRepository = questionRepository ?? throw new ArgumentNullException(nameof(questionRepository));
        }

        public async Task SeedAsync(DataSeedContext context)
        {
            if (await this.questionRepository.GetCountAsync() <= 0)
            {
                await this.questionRepository.InsertAsync(
                    new QuestionThread(Guid.NewGuid(), QuestionStatus.Active, "Préfèrez-vous le vert ou le bleu ?", true)
                    {
                    },
                    autoSave: true);

                await this.questionRepository.InsertAsync(
                    new QuestionThread(Guid.NewGuid(), QuestionStatus.Active, "Quel est le sens de la vie ?", false, audience: new QuestionAudience(ageMin: 18))
                    {
                    },
                    autoSave: true);
            }
        }
    }
}