﻿// <copyright file="LeskaAppDbMigrationService.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Data
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Logging.Abstractions;
    using Volo.Abp.Data;
    using Volo.Abp.DependencyInjection;
    using Volo.Abp.Identity;
    using Volo.Abp.MultiTenancy;
    using Volo.Abp.TenantManagement;

    public class LeskaAppDbMigrationService : ITransientDependency
    {
        public ILogger<LeskaAppDbMigrationService> Logger { get; set; }

        private readonly IDataSeeder dataSeeder;
        private readonly IEnumerable<ILeskaAppDbSchemaMigrator> dbSchemaMigrators;
        private readonly ITenantRepository tenantRepository;
        private readonly ICurrentTenant currentTenant;

        public LeskaAppDbMigrationService(
            IDataSeeder dataSeeder,
            IEnumerable<ILeskaAppDbSchemaMigrator> dbSchemaMigrators,
            ITenantRepository tenantRepository,
            ICurrentTenant currentTenant)
        {
            this.dataSeeder = dataSeeder;
            this.dbSchemaMigrators = dbSchemaMigrators;
            this.tenantRepository = tenantRepository;
            this.currentTenant = currentTenant;

            this.Logger = NullLogger<LeskaAppDbMigrationService>.Instance;
        }

        public async Task MigrateAsync()
        {

            this.Logger.LogInformation("Started database migrations...");

            await this.MigrateDatabaseSchemaAsync();
            await this.SeedDataAsync();

            this.Logger.LogInformation($"Successfully completed host database migrations.");

            var tenants = await this.tenantRepository.GetListAsync(includeDetails: true);

            var migratedDatabaseSchemas = new HashSet<string>();
            foreach (var tenant in tenants)
            {
                using (this.currentTenant.Change(tenant.Id))
                {
                    if (tenant.ConnectionStrings.Any())
                    {
                        var tenantConnectionStrings = tenant.ConnectionStrings
                            .Select(x => x.Value)
                            .ToList();

                        if (!migratedDatabaseSchemas.IsSupersetOf(tenantConnectionStrings))
                        {
                            await this.MigrateDatabaseSchemaAsync(tenant);

                            migratedDatabaseSchemas.AddIfNotContains(tenantConnectionStrings);
                        }
                    }

                    await this.SeedDataAsync(tenant);
                }

                this.Logger.LogInformation($"Successfully completed {tenant.Name} tenant database migrations.");
            }

            this.Logger.LogInformation("Successfully completed all database migrations.");
            this.Logger.LogInformation("You can safely end this process...");
        }

        private async Task MigrateDatabaseSchemaAsync(Tenant tenant = null)
        {
            this.Logger.LogInformation(
                $"Migrating schema for {(tenant == null ? "host" : tenant.Name + " tenant")} database...");

            foreach (var migrator in this.dbSchemaMigrators)
            {
                await migrator.MigrateAsync();
            }
        }

        private async Task SeedDataAsync(Tenant tenant = null)
        {
            this.Logger.LogInformation($"Executing {(tenant == null ? "host" : tenant.Name + " tenant")} database seed...");

            await this.dataSeeder.SeedAsync(new DataSeedContext(tenant?.Id)
                .WithProperty(IdentityDataSeedContributor.AdminEmailPropertyName, IdentityDataSeedContributor.AdminEmailDefaultValue)
                .WithProperty(IdentityDataSeedContributor.AdminPasswordPropertyName, IdentityDataSeedContributor.AdminPasswordDefaultValue));
        }

    }
}
