﻿// <copyright file="NullLeskaAppDbSchemaMigrator.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Data
{
    using System.Threading.Tasks;
    using Volo.Abp.DependencyInjection;

    /* This is used if database provider does't define
     * ILeskaAppDbSchemaMigrator implementation.
     */
    public class NullLeskaAppDbSchemaMigrator : ILeskaAppDbSchemaMigrator, ITransientDependency
    {
        public Task MigrateAsync()
        {
            return Task.CompletedTask;
        }
    }
}