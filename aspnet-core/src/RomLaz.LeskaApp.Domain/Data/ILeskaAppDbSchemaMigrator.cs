﻿// <copyright file="ILeskaAppDbSchemaMigrator.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Data
{
    using System.Threading.Tasks;

    public interface ILeskaAppDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}
