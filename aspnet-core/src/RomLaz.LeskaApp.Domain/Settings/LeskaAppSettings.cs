﻿// <copyright file="LeskaAppSettings.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Settings
{
    public static class LeskaAppSettings
    {
        private const string Prefix = "LeskaApp";

        //Add your own setting names here. Example:
        //public const string MySetting1 = Prefix + ".MySetting1";
    }
}