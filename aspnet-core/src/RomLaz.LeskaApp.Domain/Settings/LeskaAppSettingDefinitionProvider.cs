﻿// <copyright file="LeskaAppSettingDefinitionProvider.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Settings
{
    using Volo.Abp.Settings;

    public class LeskaAppSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            //Define your own settings here. Example:
            //context.Add(new SettingDefinition(LeskaAppSettings.MySetting1));
        }
    }
}
