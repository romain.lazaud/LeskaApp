﻿// <copyright file="LeskaAppDomainModule.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp
{
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.DependencyInjection.Extensions;
    using RomLaz.LeskaApp.MultiTenancy;
    using Volo.Abp.AuditLogging;
    using Volo.Abp.BackgroundJobs;
    using Volo.Abp.Emailing;
    using Volo.Abp.FeatureManagement;
    using Volo.Abp.Identity;
    using Volo.Abp.IdentityServer;
    using Volo.Abp.Modularity;
    using Volo.Abp.MultiTenancy;
    using Volo.Abp.PermissionManagement.Identity;
    using Volo.Abp.PermissionManagement.IdentityServer;
    using Volo.Abp.SettingManagement;
    using Volo.Abp.TenantManagement;

    [DependsOn(
        typeof(LeskaAppDomainSharedModule),
        typeof(AbpAuditLoggingDomainModule),
        typeof(AbpBackgroundJobsDomainModule),
        typeof(AbpFeatureManagementDomainModule),
        typeof(AbpIdentityDomainModule),
        typeof(AbpPermissionManagementDomainIdentityModule),
        typeof(AbpIdentityServerDomainModule),
        typeof(AbpPermissionManagementDomainIdentityServerModule),
        typeof(AbpSettingManagementDomainModule),
        typeof(AbpTenantManagementDomainModule),
        typeof(AbpEmailingModule))]
    public class LeskaAppDomainModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            this.Configure<AbpMultiTenancyOptions>(options =>
            {
                options.IsEnabled = MultiTenancyConsts.IsEnabled;
            });

#if DEBUG
            context.Services.Replace(ServiceDescriptor.Singleton<IEmailSender, NullEmailSender>());
#endif
        }
    }
}
