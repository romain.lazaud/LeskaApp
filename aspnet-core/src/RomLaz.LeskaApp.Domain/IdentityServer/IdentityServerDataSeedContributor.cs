// <copyright file="IdentityServerDataSeedContributor.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.IdentityServer
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using IdentityServer4.Models;
    using Microsoft.Extensions.Configuration;
    using Volo.Abp.Authorization.Permissions;
    using Volo.Abp.Data;
    using Volo.Abp.DependencyInjection;
    using Volo.Abp.Guids;
    using Volo.Abp.IdentityServer.ApiResources;
    using Volo.Abp.IdentityServer.ApiScopes;
    using Volo.Abp.IdentityServer.Clients;
    using Volo.Abp.IdentityServer.IdentityResources;
    using Volo.Abp.MultiTenancy;
    using Volo.Abp.PermissionManagement;
    using Volo.Abp.Uow;
    using ApiResource = Volo.Abp.IdentityServer.ApiResources.ApiResource;
    using ApiScope = Volo.Abp.IdentityServer.ApiScopes.ApiScope;
    using Client = Volo.Abp.IdentityServer.Clients.Client;

    public class IdentityServerDataSeedContributor
        : IDataSeedContributor, ITransientDependency
    {
        private readonly IApiResourceRepository apiResourceRepository;
        private readonly IApiScopeRepository apiScopeRepository;
        private readonly IClientRepository clientRepository;
        private readonly IIdentityResourceDataSeeder identityResourceDataSeeder;
        private readonly IGuidGenerator guidGenerator;
        private readonly IPermissionDataSeeder permissionDataSeeder;
        private readonly IConfiguration configuration;
        private readonly ICurrentTenant currentTenant;

        public IdentityServerDataSeedContributor(
            IClientRepository clientRepository,
            IApiResourceRepository apiResourceRepository,
            IApiScopeRepository apiScopeRepository,
            IIdentityResourceDataSeeder identityResourceDataSeeder,
            IGuidGenerator guidGenerator,
            IPermissionDataSeeder permissionDataSeeder,
            IConfiguration configuration,
            ICurrentTenant currentTenant)
        {
            this.clientRepository = clientRepository;
            this.apiResourceRepository = apiResourceRepository;
            this.apiScopeRepository = apiScopeRepository;
            this.identityResourceDataSeeder = identityResourceDataSeeder;
            this.guidGenerator = guidGenerator;
            this.permissionDataSeeder = permissionDataSeeder;
            this.configuration = configuration;
            this.currentTenant = currentTenant;
        }

        [UnitOfWork]
        public virtual async Task SeedAsync(DataSeedContext context)
        {
            using (this.currentTenant.Change(context?.TenantId))
            {
                await this.identityResourceDataSeeder.CreateStandardResourcesAsync();
                await this.CreateApiResourcesAsync();
                await this.CreateApiScopesAsync();
                await this.CreateClientsAsync();
            }
        }

        private async Task CreateApiScopesAsync()
        {
            await this.CreateApiScopeAsync("LeskaApp");
        }

        private async Task CreateApiResourcesAsync()
        {
            var commonApiUserClaims = new[]
            {
                "email",
                "email_verified",
                "name",
                "phone_number",
                "phone_number_verified",
                "role",
            };

            await this.CreateApiResourceAsync("LeskaApp", commonApiUserClaims);
        }

        private async Task<ApiResource> CreateApiResourceAsync(string name, IEnumerable<string> claims)
        {
            var apiResource = await this.apiResourceRepository.FindByNameAsync(name);
            if (apiResource == null)
            {
                apiResource = await this.apiResourceRepository.InsertAsync(
                    new ApiResource(
                        this.guidGenerator.Create(),
                        name,
                        name + " API"),
                    autoSave: true);
            }

            foreach (var claim in claims)
            {
                if (apiResource.FindClaim(claim) == null)
                {
                    apiResource.AddUserClaim(claim);
                }
            }

            return await this.apiResourceRepository.UpdateAsync(apiResource);
        }

        private async Task<ApiScope> CreateApiScopeAsync(string name)
        {
            var apiScope = await this.apiScopeRepository.FindByNameAsync(name);
            if (apiScope == null)
            {
                apiScope = await this.apiScopeRepository.InsertAsync(
                    new ApiScope(
                        this.guidGenerator.Create(),
                        name,
                        name + " API"),
                    autoSave: true);
            }

            return apiScope;
        }

        private async Task CreateClientsAsync()
        {
            var commonScopes = new[]
            {
                "email",
                "openid",
                "profile",
                "role",
                "phone",
                "address",
                "LeskaApp",
            };

            var configurationSection = this.configuration.GetSection("IdentityServer:Clients");

            // Console Test / Angular Client
            var consoleAndAngularClientId = configurationSection["LeskaApp_App:ClientId"];

            if (!consoleAndAngularClientId.IsNullOrWhiteSpace())
            {
                var webClientRootUrl = configurationSection["LeskaApp_App:RootUrl"]?.TrimEnd('/');

                await this.CreateClientAsync(
                    name: consoleAndAngularClientId,
                    scopes: commonScopes,
                    grantTypes: new[] { "password", "client_credentials", "authorization_code" },
                    secret: (configurationSection["LeskaApp_App:ClientSecret"] ?? "1q2w3e*").Sha256(),
                    requireClientSecret: false,
                    redirectUri: webClientRootUrl,
                    postLogoutRedirectUri: webClientRootUrl,
                    corsOrigins: new[] { webClientRootUrl.RemovePostFix("/") });
            }

            // Swagger Client
            var swaggerClientId = configurationSection["LeskaApp_Swagger:ClientId"];
            if (!swaggerClientId.IsNullOrWhiteSpace())
            {
                var swaggerRootUrl = configurationSection["LeskaApp_Swagger:RootUrl"].TrimEnd('/');

                await this.CreateClientAsync(
                    name: swaggerClientId,
                    scopes: commonScopes,
                    grantTypes: new[] { "authorization_code" },
                    secret: configurationSection["LeskaApp_Swagger:ClientSecret"]?.Sha256(),
                    requireClientSecret: false,
                    redirectUri: $"{swaggerRootUrl}/swagger/oauth2-redirect.html",
                    corsOrigins: new[] { swaggerRootUrl.RemovePostFix("/") });
            }
        }

        private async Task<Client> CreateClientAsync(
            string name,
            IEnumerable<string> scopes,
            IEnumerable<string> grantTypes,
            string secret = null,
            string redirectUri = null,
            string postLogoutRedirectUri = null,
            string frontChannelLogoutUri = null,
            bool requireClientSecret = true,
            bool requirePkce = false,
            IEnumerable<string> permissions = null,
            IEnumerable<string> corsOrigins = null)
        {
            var client = await this.clientRepository.FindByClientIdAsync(name);
            if (client == null)
            {
                client = await this.clientRepository.InsertAsync(
                    new Client(
                        this.guidGenerator.Create(),
                        name)
                    {
                        ClientName = name,
                        ProtocolType = "oidc",
                        Description = name,
                        AlwaysIncludeUserClaimsInIdToken = true,
                        AllowOfflineAccess = true,
                        AbsoluteRefreshTokenLifetime = 31536000,
                        AccessTokenLifetime = 31536000,
                        AuthorizationCodeLifetime = 300,
                        IdentityTokenLifetime = 300,
                        RequireConsent = false,
                        FrontChannelLogoutUri = frontChannelLogoutUri,
                        RequireClientSecret = requireClientSecret,
                        RequirePkce = requirePkce,
                    },
                    autoSave: true);
            }

            foreach (var scope in scopes)
            {
                if (client.FindScope(scope) == null)
                {
                    client.AddScope(scope);
                }
            }

            foreach (var grantType in grantTypes)
            {
                if (client.FindGrantType(grantType) == null)
                {
                    client.AddGrantType(grantType);
                }
            }

            if (!secret.IsNullOrEmpty() && client.FindSecret(secret) == null)
            {
                client.AddSecret(secret);
            }

            if (redirectUri != null && client.FindRedirectUri(redirectUri) == null)
            {
                client.AddRedirectUri(redirectUri);
            }

            if (postLogoutRedirectUri != null && client.FindPostLogoutRedirectUri(postLogoutRedirectUri) == null)
            {
                client.AddPostLogoutRedirectUri(postLogoutRedirectUri);
            }

            if (permissions != null)
            {
                await this.permissionDataSeeder.SeedAsync(
                    ClientPermissionValueProvider.ProviderName,
                    name,
                    permissions,
                    null);
            }

            if (corsOrigins != null)
            {
                foreach (var origin in corsOrigins)
                {
                    if (!origin.IsNullOrWhiteSpace() && client.FindCorsOrigin(origin) == null)
                    {
                        client.AddCorsOrigin(origin);
                    }
                }
            }

            return await this.clientRepository.UpdateAsync(client);
        }
    }
}