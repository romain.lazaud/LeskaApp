﻿// <copyright file="ThreadItem.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Questions
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using Volo.Abp;
    using Volo.Abp.Domain.Entities;

    public class ThreadItem
        : Entity<int>
    {
        internal ThreadItem(int id, Guid threadId, [NotNull] string content, [CanBeNull] int? parentId)
            : base(id)
        {
            this.RootId = threadId;
            this.Content = content;
            this.ParentId = parentId;
        }

        protected ThreadItem()
        {
        }

        public virtual Guid RootId { get; }

        public virtual int? ParentId { get; }

        public virtual string Content { get; protected set; }

        public virtual ICollection<Guid> LikedByUsers { get; protected set; }

        public override object[] GetKeys()
        {
            return new object[] { this.Id, this.RootId };
        }

        internal ThreadItem ChangeContent([NotNull] string newContent)
        {
            this.SetContent(newContent);

            return this;
        }

        private void SetContent([NotNull] string newContent)
        {
            this.Content = Check.NotNullOrWhiteSpace(
                newContent,
                nameof(newContent),
                maxLength: QuestionConsts.MaxContentLength);
        }
    }
}