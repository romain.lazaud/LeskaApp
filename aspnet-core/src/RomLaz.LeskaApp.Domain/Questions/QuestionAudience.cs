﻿// <copyright file="QuestionAudience.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Questions
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using RomLaz.LeskaApp.Profile;
    using Volo.Abp.Domain.Values;

    public class QuestionAudience
        : ValueObject
    {
        internal QuestionAudience([CanBeNull] int? ageMin = null, [CanBeNull] int? ageMax = null, [CanBeNull] Gender? gender = null)
        {
            this.AgeMin = ageMin;
            this.AgeMax = ageMax;
            this.Gender = gender;
            this.Countries = new List<string>();
        }

        public int? AgeMin { get; }

        public int? AgeMax { get; }

        public Gender? Gender { get; }

        public List<string> Countries { get; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.AgeMin;
            yield return this.AgeMax;
            yield return this.Gender;
            yield return this.Countries;
        }
    }
}