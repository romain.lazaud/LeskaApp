﻿namespace RomLaz.LeskaApp.Questions
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using Volo.Abp.Domain.Values;

    public class ThreadItemArgumentSource
        : ValueObject
    {
        protected ThreadItemArgumentSource()
        {
        }

        internal ThreadItemArgumentSource([NotNull] Guid threadId, [NotNull] int threadItemId, [NotNull] string url)
        {
            this.ThreadId = threadId;
            this.ThreadItemId = threadItemId;
            this.Url = url;
        }

        public Guid ThreadId { get; }

        public int ThreadItemId { get; }

        public string Url { get; protected set; }

        public bool IsValidated { get; protected set; }

        public void SetIsValidated(bool isValidated)
        {
            this.IsValidated = isValidated;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.ThreadId;
            yield return this.ThreadItemId;
            yield return this.Url;
        }
    }
}