﻿// <copyright file="QuestionThread.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Questions
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using Volo.Abp;
    using Volo.Abp.Domain.Entities.Auditing;

    public class QuestionThread
        : FullAuditedAggregateRoot<Guid>
    {
        internal QuestionThread(
            Guid id,
            QuestionStatus status,
            [NotNull] ThreadItem root,
            bool areAnswersLocked,
            [CanBeNull] string description = null,
            [CanBeNull] QuestionAudience audience = null)
            : base(id)
        {
            this.Status = status;
            this.AreAnswersLocked = areAnswersLocked;
            this.Root = root;
            this.Description = description;
            this.Audience = audience;
            this.Answers = new List<ThreadItem>();
        }

        protected QuestionThread()
        {
        }

        public QuestionStatus Status { get; protected set; }

        public virtual List<ThreadItem> Answers { get; protected set; }

        public virtual bool AreAnswersLocked { get; protected set; }

        public virtual ThreadItem Root { get; protected set; }

        public virtual string Description { get; protected set; }

        public virtual QuestionAudience Audience { get; protected set; }

        internal QuestionThread ChangeStatus(QuestionStatus newStatus)
        {
            this.Status = newStatus;

            return this;
        }

        internal QuestionThread ChangeDescription(string description)
        {
            this.SetDescription(description);

            return this;
        }

        private void SetDescription(string description)
        {
            this.Description = Check.NotNullOrWhiteSpace(
                description,
                nameof(description),
                maxLength: QuestionConsts.MaxDescriptionLength);
        }
    }
}