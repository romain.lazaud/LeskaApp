﻿// <copyright file="LeskaAppConsts.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp
{
    public static class LeskaAppConsts
    {
        public const string DbTablePrefix = "App";

        public const string DbSchema = null;
    }
}
