﻿namespace RomLaz.LeskaApp.Questions
{
    public enum QuestionStatus
    {
        NotStarted,
        Active,
        Closed,
    }
}