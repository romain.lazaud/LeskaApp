﻿// <copyright file="QuestionConsts.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Questions
{
    public static class QuestionConsts
    {
        public const int MaxContentLength = 128;
        public const int MaxDescriptionLength = 512;

        public static class Audience
        {
            public const int MinAge = 6;
            public const int MaxAge = 120;
        }
    }
}