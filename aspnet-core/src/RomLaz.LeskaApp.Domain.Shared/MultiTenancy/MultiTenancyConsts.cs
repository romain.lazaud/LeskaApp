﻿// <copyright file="MultiTenancyConsts.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.MultiTenancy
{
    public static class MultiTenancyConsts
    {
        /* Enable/disable multi-tenancy easily in a single point.
         * If you will never need to multi-tenancy, you can remove
         * related modules and code parts, including this file.
         */
        public const bool IsEnabled = true;
    }
}
