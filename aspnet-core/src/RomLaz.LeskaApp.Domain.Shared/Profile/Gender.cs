﻿// <copyright file="Gender.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Profile
{
    public enum Gender
    {
        Undefined,
        Male,
        Female,
    }
}