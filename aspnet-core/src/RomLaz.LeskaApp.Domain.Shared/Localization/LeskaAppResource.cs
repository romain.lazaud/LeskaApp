﻿// <copyright file="LeskaAppResource.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Localization
{
    using Volo.Abp.Localization;

    [LocalizationResourceName("LeskaApp")]
    public class LeskaAppResource
    {

    }
}