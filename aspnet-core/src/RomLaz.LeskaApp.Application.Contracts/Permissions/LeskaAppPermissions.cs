﻿// <copyright file="LeskaAppPermissions.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Permissions
{
    public static class LeskaAppPermissions
    {
        public const string GroupName = "LeskaApp";

        //Add your own permission names. Example:
        //public const string MyPermission1 = GroupName + ".MyPermission1";
    }
}