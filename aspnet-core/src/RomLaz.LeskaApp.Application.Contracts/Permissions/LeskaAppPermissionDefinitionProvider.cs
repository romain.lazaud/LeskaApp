﻿// <copyright file="LeskaAppPermissionDefinitionProvider.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Permissions
{
    using RomLaz.LeskaApp.Localization;
    using Volo.Abp.Authorization.Permissions;
    using Volo.Abp.Localization;

    public class LeskaAppPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(LeskaAppPermissions.GroupName);
            //Define your own permissions here. Example:
            //myGroup.AddPermission(LeskaAppPermissions.MyPermission1, L("Permission:MyPermission1"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<LeskaAppResource>(name);
        }
    }
}
