﻿// <copyright file="LeskaAppApplicationContractsModule.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp
{
    using Volo.Abp.Account;
    using Volo.Abp.FeatureManagement;
    using Volo.Abp.Identity;
    using Volo.Abp.Modularity;
    using Volo.Abp.ObjectExtending;
    using Volo.Abp.PermissionManagement;
    using Volo.Abp.SettingManagement;
    using Volo.Abp.TenantManagement;

    [DependsOn(
        typeof(LeskaAppDomainSharedModule),
        typeof(AbpAccountApplicationContractsModule),
        typeof(AbpFeatureManagementApplicationContractsModule),
        typeof(AbpIdentityApplicationContractsModule),
        typeof(AbpPermissionManagementApplicationContractsModule),
        typeof(AbpSettingManagementApplicationContractsModule),
        typeof(AbpTenantManagementApplicationContractsModule),
        typeof(AbpObjectExtendingModule))]
    public class LeskaAppApplicationContractsModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            LeskaAppDtoExtensions.Configure();
        }
    }
}
