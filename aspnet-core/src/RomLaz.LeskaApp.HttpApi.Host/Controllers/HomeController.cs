﻿// <copyright file="HomeController.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Volo.Abp.AspNetCore.Mvc;

    public class HomeController : AbpController
    {
        public ActionResult Index()
        {
            return this.Redirect("~/swagger");
        }
    }
}
