﻿// <copyright file="DbMigratorHostedService.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.DbMigrator
{
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using RomLaz.LeskaApp.Data;
    using Serilog;
    using Volo.Abp;

    public class DbMigratorHostedService
        : IHostedService
    {
        private readonly IHostApplicationLifetime hostApplicationLifetime;
        private readonly IConfiguration configuration;

        public DbMigratorHostedService(IHostApplicationLifetime hostApplicationLifetime, IConfiguration configuration)
        {
            this.hostApplicationLifetime = hostApplicationLifetime;
            this.configuration = configuration;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            using (var application = AbpApplicationFactory.Create<LeskaAppDbMigratorModule>(options =>
            {
                options.Services.ReplaceConfiguration(this.configuration);
                options.UseAutofac();
                options.Services.AddLogging(c => c.AddSerilog());
            }))
            {
                application.Initialize();

                await application
                    .ServiceProvider
                    .GetRequiredService<LeskaAppDbMigrationService>()
                    .MigrateAsync();

                application.Shutdown();

                this.hostApplicationLifetime.StopApplication();
            }
        }

        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
    }
}