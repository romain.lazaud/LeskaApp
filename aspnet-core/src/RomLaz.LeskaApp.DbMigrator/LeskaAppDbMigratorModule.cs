// <copyright file="LeskaAppDbMigratorModule.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.DbMigrator
{
    using RomLaz.LeskaApp.MongoDB;
    using Volo.Abp.Autofac;
    using Volo.Abp.BackgroundJobs;
    using Volo.Abp.Modularity;

    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(LeskaAppMongoDbModule),
        typeof(LeskaAppApplicationContractsModule))]
    public class LeskaAppDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            this.Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
        }
    }
}
