﻿// <copyright file="LeskaAppController.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Controllers
{
    using RomLaz.LeskaApp.Localization;
    using Volo.Abp.AspNetCore.Mvc;

    /* Inherit your controllers from this class.
     */
    public abstract class LeskaAppController : AbpControllerBase
    {
        protected LeskaAppController()
        {
            this.LocalizationResource = typeof(LeskaAppResource);
        }
    }
}