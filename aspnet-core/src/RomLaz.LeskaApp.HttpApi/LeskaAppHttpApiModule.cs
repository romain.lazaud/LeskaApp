﻿// <copyright file="LeskaAppHttpApiModule.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp
{
    using global::Localization.Resources.AbpUi;
    using RomLaz.LeskaApp.Localization;
    using Volo.Abp.Account;
    using Volo.Abp.FeatureManagement;
    using Volo.Abp.Identity;
    using Volo.Abp.Localization;
    using Volo.Abp.Modularity;
    using Volo.Abp.PermissionManagement.HttpApi;
    using Volo.Abp.SettingManagement;
    using Volo.Abp.TenantManagement;

    [DependsOn(
        typeof(LeskaAppApplicationContractsModule),
        typeof(AbpAccountHttpApiModule),
        typeof(AbpIdentityHttpApiModule),
        typeof(AbpPermissionManagementHttpApiModule),
        typeof(AbpTenantManagementHttpApiModule),
        typeof(AbpFeatureManagementHttpApiModule),
        typeof(AbpSettingManagementHttpApiModule))]
    public class LeskaAppHttpApiModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            this.ConfigureLocalization();
        }

        private void ConfigureLocalization()
        {
            this.Configure<AbpLocalizationOptions>(options =>
            {
                options.Resources
                    .Get<LeskaAppResource>()
                    .AddBaseTypes(
                        typeof(AbpUiResource));
            });
        }
    }
}
