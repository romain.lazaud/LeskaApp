﻿// <copyright file="TestModel.cs" company="RomLaz">
// Copyright (c) RomLaz. All rights reserved.
// </copyright>

namespace RomLaz.LeskaApp.Models.Test
{
    using System;

    public class TestModel
    {
        public string Name { get; set; }

        public DateTime BirthDate { get; set; }
    }
}